# CentOS systemd Images

This image is available on [Docker Hub](https://hub.docker.com/r/rbm0407/centos-systemd)

## Base OS
 - CentOS

## License

Licensed under the Apache License V2.0. See the [LICENSE file](LICENSE) for details.
