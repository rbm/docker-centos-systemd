#!/usr/bin/env bash

docker build --network=host -t rbm0407/centos-systemd:latest latest && \
    docker push rbm0407/centos-systemd:latest

docker build --network=host -t rbm0407/centos-systemd:7 7 && \
    docker push rbm0407/centos-systemd:7
